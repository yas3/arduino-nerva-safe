#include <Arduino.h>

#include <Crypto.h>
#include <AES.h>
#include <string.h>
#include <EEPROM.h>

String inputString = "";     // a String to hold incoming data
bool stringComplete = false; // whether the string is complete
uint8_t key[32];

AESSmall256 aes256;

byte buffer[7][16];
byte buffer_dec[7][16];

byte view_key[4][16];
byte spend_key[4][16];
byte address[7][16];

void dec(BlockCipher *cipher, uint8_t *key)
{
  cipher->setKey(key, cipher->keySize());
  int add = 0;
  for (int i = 0; i < 7; i++)
  {
    memset(buffer[i], 0, sizeof(buffer[i]));
    memset(buffer_dec[i], 0, sizeof(buffer_dec[i]));
  }
  Serial.println("\nAddress:");
  for (int i = 0; i < 7; i++)
  {
    for (int j = 0; j < 16; j++)
    {
      buffer[i][j] = EEPROM.read(add);
      add++;
    }
    cipher->decryptBlock(buffer_dec[i], buffer[i]);
  }
  Serial.print((char *)buffer_dec);

  for (int i = 0; i < 7; i++)
  {
    memset(buffer[i], 0, sizeof(buffer[i]));
    memset(buffer_dec[i], 0, sizeof(buffer_dec[i]));
  }
  Serial.println("\nSpend:");
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 16; j++)
    {
      buffer[i][j] = EEPROM.read(add);
      add++;
    }
    cipher->decryptBlock(buffer_dec[i], buffer[i]);
  }
  Serial.print((char *)buffer_dec);

  for (int i = 0; i < 7; i++)
  {
    memset(buffer[i], 0, sizeof(buffer[i]));
    memset(buffer_dec[i], 0, sizeof(buffer_dec[i]));
  }
  Serial.println("\nView:");
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 16; j++)
    {
      buffer[i][j] = EEPROM.read(add);
      add++;
    }
    cipher->decryptBlock(buffer_dec[i], buffer[i]);
  }
  Serial.print((char *)buffer_dec);
  Serial.println("\nMemory used:"+String(add));
}

void enc(BlockCipher *cipher, uint8_t *key)
{
  int add = 0;
  cipher->setKey(key, cipher->keySize());
  for (int i = 0; i < 7; i++)
  {
    memset(buffer[i], 0, sizeof(buffer[i]));
  }
  for (int i = 0; i < 7; i++)
  {
    cipher->encryptBlock(buffer[i], address[i]);
    for (int j = 0; j < 16; j++)
    {
      EEPROM.update(add, buffer[i][j]);
      add++;
    }
  }

  for (int i = 0; i < 7; i++)
  {
    memset(buffer[i], 0, sizeof(buffer[i]));
  }

  for (int i = 0; i < 4; i++)
  {
    cipher->encryptBlock(buffer[i], spend_key[i]);
    for (int j = 0; j < 16; j++)
    {
      EEPROM.update(add, buffer[i][j]);
      add++;
    }
  }

  for (int i = 0; i < 7; i++)
  {
    memset(buffer[i], 0, sizeof(buffer[i]));
  }

  for (int i = 0; i < 4; i++)
  {
    cipher->encryptBlock(buffer[i], view_key[i]);
    for (int j = 0; j < 16; j++)
    {
      EEPROM.update(add, buffer[i][j]);
      add++;
    }
  }
  Serial.println("\nMemory used:"+String(add));
}
int state = 0;
void serialEvent()
{
  char inChar;
  while (Serial.available())
  {
    inChar = (char)Serial.read();
    if (inChar == '\n' || inChar == '\r')
    {
      inputString +='\0';
      stringComplete = true;
    }
    else
    {
      inputString += inChar;
      if(state==4 || state==10){
        Serial.print("*");
      }else{
        Serial.print(inChar);
      }
      
    }
  }
}

void setup()
{
  delay(1000);
  Serial.begin(9600);
  inputString.reserve(200);

  for (int i = 0; i < 7; i++)
  {
    memset(address[i], 0, sizeof(address[i]));
    memset(buffer_dec[i], 0, sizeof(buffer_dec[i]));
    memset(buffer[i], 0, sizeof(buffer[i]));
  }
  delay(5000);
  Serial.println("\nEnter Option:");
  Serial.println("*view");
  Serial.println("*save");
}

void loop()
{
  if (stringComplete)
  {
    int i = 0;
    int j = 0;

    switch (state)
    {
    case 0: //home
      if (inputString.equals("save"))
      {
        state = 10;
        Serial.println("\nEnter key:");
        
      }
      else if (inputString.equals("view"))
      {
        state = 4;
        Serial.println("\nEnter key:");
      }
      break;
    case 1: //save address
      for (int k = 0; k < 97; k++)
      {
        address[i][j] = inputString.charAt(k);
        j++;
        if (j >= 16)
        {
          i++;
          j = 0;
        }
      }

      Serial.println("\nEnter view:");
      state = 2;

      break;
    case 2: //save view
      for (int k = 0; k < 64; k++)
      {
        view_key[i][j] = inputString.charAt(k);
        j++;
        if (j >= 16)
        {
          i++;
          j = 0;
        }
      }

      Serial.println("\nEnter spend:");
      state = 3;
      break;
    case 3: //save spend
      for (int k = 0; k < 64; k++)
      {
        spend_key[i][j] = inputString.charAt(k);
        j++;
        if (j >= 16)
        {
          i++;
          j = 0;
        }
      }
      enc(&aes256, key);
      Serial.println("\nSaved.");
      state = 0;
      break;
    case 4: //read
      inputString.toCharArray(key,32);
      state = 5;
      break;
    case 5: //read
      dec(&aes256, key);
      state = 0;
      break;
    case 10: //save method get key
      inputString.toCharArray(key,32);
      state = 1;
      Serial.println("\nEnter Address:");
      break;
    default:
      state=0;
    break;
    }

    inputString = "";
    stringComplete = false;
    Serial.flush();
    while (Serial.available())
    {
      char inChar = (char)Serial.read();
    }
  }
}